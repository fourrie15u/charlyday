<?php


namespace charlyday\vue;

use charlyday\controler\CreneauControler;
use charlyday\model\Authentication;
use Slim\Slim;


class VueCreneau extends Vue {
    const SEMAINE = 1;
    const NEW = 2;

    public function render($sel, $arg) {
        $head = parent::renduTitre();
        $menu = parent::renduMenu();
        $foot = parent::rendufooter();
        $res = "";
        switch ($sel) {
            case self::SEMAINE :
                $res .= $this->renderSemaine($arg);
                break;
            case self::NEW :
                $res .= $this->renderNew();
                break;
        }

        echo $head . $menu . "<div style='background: white; padding: 25px'>" . $res . "</div>" . $foot;
    }

    private function renderSemaine($arg) {
        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getRootUri();
        $admin = Authentication::checkAccessRights();
        $prevSem = CreneauControler::prevCreneauSem($arg["cycle"], $arg["semaine"]);
        $nextSem = CreneauControler::nextCreneauSem($arg["cycle"], $arg["semaine"]);
        $prevCycle = CreneauControler::prevCreneauCycle($arg["cycle"], $arg["semaine"]);
        $nextCycle = CreneauControler::nextCreneauCycle($arg["cycle"], $arg["semaine"]);
        $ajoutbesoin = "";
        $creid = "";
        $desactiv = "";
        if ($admin)
            $desactiv = "<button type=\"submit\" class=\"btn btn-warning\" form='des' name='des' value=$creid>Desactive Creneau</button>
<form id='des' method='post' action='$url/desactive'>";
        $ajoutbesoin = "";
        if ($admin)
            $ajoutbesoin = "<button type='button' class='btn btn-warning' data-dismiss=\"modal\" data-toggle=\"modal\" data-target=\"#exampleModal\" >Ajouter un besoin</button>
<div class=\"modal fade\" id=\"exampleModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalLabel\" aria-hidden=\"true\">
  <div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLabel\">Modal title</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        ...
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
";
        $creneaux = $arg["creneaux"];

        $jours = [1 => "", 2 => "", 3 => "", 4 => "", 5 => "", 6 => "", 7 => ""];
        foreach ($creneaux as $creneau) {
            $jourID = $creneau->heure . "_" . $creneau->jour;


            $roles = $this->generateRoles($creneau);
            $jours[$creneau->jour] .= "<button type='button' class='list-group-item list-group-item-action' data-toggle=\"modal\" data-target=\"#exampleModalScrollable$jourID\">De " . $creneau->heure . " h à " . ($creneau->heure + 1) . "h</button>\n
<!-- Modal -->
<div class=\"modal fade text-dark\" id=\"exampleModalScrollable$jourID\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalScrollableTitle\" aria-hidden=\"true\" style=\"z-index: 1600;\">
  <div class=\"modal-dialog modal-dialog-scrollable\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalScrollableTitle\">Créneau horraire</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
        <p> Choisiez votre rôle</p>
<form id='role' method='post' action='$url/inscription'>
<div class=\"form-group\">
  <label class=\"col-md-4 control-label\" for=\"radios\">Rôles disponibles</label>
  <div class=\"col-md-9\">
 $roles
  </div>
 </form>
      </div>    
      <div class=\"modal-footer\">
       $ajoutbesoin
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"submit\" class=\"btn btn-primary\" form='role' name='save' value=$creneau->id>Save changes</button>
      </div>
    </div>
  </div>
</div>
</div>

";
        }
        $res = "
<div class='boutonsPrecSuiv' STYLE='justify-content: center'>
<button type='button' class='btn btn-light' disabled>Cycle: " . $arg['cycle'] . " - Semaine: " . $arg['semaine'] . " </button>
</div>";

        $res .= "<div class='boutonsPrecSuiv'>
<a href='$prevCycle' <button type='button' class='btn btn-danger cycleButtonNav'>← Cycle précédent</button></a>
<a href='$prevSem' <button type='button' class='btn btn-warning semButtonNav'>← Semaine précédente</button></a>
<a href='$nextSem' <button type='button' class='btn btn-warning semButtonNav'>Semaine suivante →</button></a>
<a href='$nextCycle' <button type='button' class='btn btn-danger cycleButtonNav'>Cycle suivant →</button></a>
</div>

";
        $res .= "

<div class='card-deck'>
<div class='card text-white bg-lundi mb-3' style='min-width: 18rem;'>
  <div class='card-header'>Lundi</div>
  <div class='card-body'>
  <div class='list-group'>
    $jours[1]
    ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document.location.href='" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=1'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
  </div>
</div>
 <div class='card text-white bg-mardi mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Mardi</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[2]
    ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=2'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
</div>
</div>
 <div class='card text-white bg-mercredi mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Mercredi</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[3]
  ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=3'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
</div>
</div>
 <div class='card text-white bg-jeudi mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Jeudi</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[4]
    ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=4'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
</div>
</div>
 <div class='card text-white bg-vendredi mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Vendredi</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[5]
  ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=5'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
</div>
</div>
 <div class='card text-white bg-samedi mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Samedi</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[6]
  ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=6'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
</div>
</div>
 <div class='card text-white bg-dimanche mb-3' style='min-width: 18rem; '>
  <div class='card-header'>Dimanche</div>
  <div class='card-body'>
  <div class='list-group'>
  $jours[7]
  ";
        if ($admin)
            $res .= "<button type='button' class='list-group-item list-group-item-action active' onClick=\"javascript:document . location . href = '" . Slim::getInstance()->urlFor("newCreneau") . "?cycle=" . $arg["cycle"] . "&semaine=" . $arg["semaine"] . "&jour=7'\">⊕ Ajouter un créneau</button>";

        $res .= "</div>
  </div>
  </div>
</div>
</div>
";
        return $res;
    }

    private function generateRoles($creneau) {
        $roles = "";
        $creneaubesoins = $creneau->creneaux;
        foreach ($creneaubesoins as $creneaubesoin) {
            if ($creneaubesoin->roleComplet == 0) {
                $label = Authentication::aLerole($creneaubesoin->role);
                if ($label != "") {
                    $roles = $roles . "<div class='radio'>
    <label for='radios-2'>
      <input type='radio' name='radios' id='radios-2' value=$creneaubesoin->idRole form='role'>
                    $label
                </label>
	</div>";
                }
            }

            return $roles;
        }
    }

    private function renderNew() {
        $url = Slim::getInstance()->urlFor("newCreneauProcess");
        return <<< FORM
<form class="form-horizontal" action="$url" method="post">
<fieldset>

<!-- Form Name -->
<legend>Nouveau créneau horraire</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cycle">Cycle</label>  
  <div class="col-md-4">
  <input id="cycle" name="cycle" type="number" min="1" placeholder="cycle" class="form-control input-md" value ="1" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="semaine">Semaine</label>
  <div class="col-md-4">
    <select id="semaine" name="semaine" class="form-control">
      <option value="A">A</option>
      <option value="B">B</option>
      <option value="C">C</option>
      <option value="D">D</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="jour">Jour</label>
  <div class="col-md-4">
    <select id="jour" name="jour" class="form-control" >
      <option value="1">Lundi</option>
      <option value="2">Mardi</option>
      <option value="3">Mercredi</option>
      <option value="4">Jeudi</option>
      <option value="5">Vendredi</option>
      <option value="6">Samedi</option>
      <option value="7">Dimanche</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="heure">Heure</label>  
  <div class="col-md-4">
  <input id="heure" name="heure" type="number" min="0" max="23" placeholder="heure" class="form-control input-md" value="0" required="">
  <span class="help-block">Heure de début du créneau</span>  
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="valider"></label>
  <div class="col-md-4">
    <button type="submit" id="valider" name="valider" class="btn btn-primary">Valider</button>
  </div>
</div>

</fieldset>
</form>

FORM;

    }
}