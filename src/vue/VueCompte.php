<?php


namespace charlyday\vue;


use charlyday\model\Authentication;
use Slim\Slim;

class VueCompte extends Vue {
    const MAIN_PAGE_COMPTE = 1;
    const PAGE_AUF = 2;
    const AUFMODIFCOMPTE = 3;
    const NORMAL = 4;
    const CRENAUX = 5;
    const ADMIN = 6;

    /**
     * VueCompte constructor.
     */
    public function __construct() { }

    public function render($sel, $arg) {
        $head = parent::renduTitre();
        $menu = parent::renduMenu();
        $foot = parent::rendufooter();
        $res = "";


        switch ($sel) {
            case self::MAIN_PAGE_COMPTE :
                $res = $this->renderPageCompte($arg);
                break;
            case self::PAGE_AUF :
                $res = $this->renderPageAuf($arg);
                break;
            case self::AUFMODIFCOMPTE:
                $res .= $this->renderAufModifCompte($arg);
                break;
            case self::CRENAUX :
                $res .= $this->renderPageCrenaux($arg);
                break;
            case self::NORMAL:
                $res .= $this->renderAufMenu($arg);
                break;
            case self::ADMIN:
                $res .= $this->renderAdmin($arg);
                break;
        }

        echo $head . $menu . $res . $foot;
    }

    private function renderPageCompte($arg) {
        $compte = $arg['compte'];
        return <<<END
 <div class="my-3 p-3 bg-white rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Sélectionner un compte</h6>
              $compte
      </div>

END;
    }

    private function renderPageAuf($arg) {
        return $arg;
    }

    private function renderAufModifCompte($arg) {
        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getPath();
        $urlLogout = $request->getRootUri() . "/logout";
        $u = $arg['user'];
        $urlmodifCompte = $request->getRootUri() . "/modifcompte";
        $adminLink = $this->generateAdminLink($url);
        return <<<END
<div class="row text-white">
  <div class="col-3">
    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
      <a class="nav-link" href="$url?page= " role="tab">Page Principal</a>
      <a class="nav-link active"  href="$url?page=modif" role="tab">Modifier compte</a>
      <a class="nav-link"  href="$url?page=crenaux" role="tab">Créneaux</a>
      $adminLink
      <a class="nav-link"  href="$urlLogout" role="tab">Déconnexion</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="v-pills-tabContent">
      <div>
         <div class="form">
          <form class="form-check" method="post" action="$urlmodifCompte">
           <div class="form-row">
      <div class="form-group col-md-6">
      <label for="name">Nom</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Nom" required value="$u->nom">
    </div>
    <div class="form-group col-md-6">
      <label for="prenom">Prenom</label>
      <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom" required value="$u->prenom">
    </div></div>
  <div class="form-row">
      <div class="form-group col-md-6">
      <label for="login">Login</label>
      <input class="form-control disabled" disabled placeholder="login" id="login" required value="$u->login">
    </div>
     <div class="form-group col-md-6">
      <label for="mail">Mail</label>
      <input type="email" class="form-control" id="mail" name="mail" placeholder="Email"  value="$u->mail" required>
    </div> </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="tel">Telephone</label>
      <input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="tel" name="tel" placeholder="Telephone"  value="$u->telephone" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Numéro de téléphone invalide' : '')" required >
    </div>
</div>
  <div class="form-row">
   <div class="form-group col-md-6">
      <label for="password">Mot de passe</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" onchange="if(this.checkValidity()) form.passwordConf.pattern = this.value;">
    </div>
    <div class="form-group col-md-6">
      <label for="passwordConf">Confirmation</label>
      <input type="password" class="form-control" id="passwordConf" name="passwordConf" placeholder="Mot de passe" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Mots de passe différents' : '');">
    </div> </div>
    <button type="submit" class="btn btn-primary">Validez</button>
          </form>
          <form action="$urlmodifCompte" method="post">
          <input type="hidden" name="_METHOD" value="DELETE" hidden/> <!--https://docs.slimframework.com/routing/delete/ -->
          <div class="col-md-6"><input type="submit" class="btn btn-danger" value="Supprimez Compte"></div>
          </form>
        </div>
</div>
    </div>
  </div>
</div>
END;
    }

    private function generateAdminLink($url, $sel = "") {
        if (Authentication::checkAccessRights()) {
            return "<a class=\"nav-link $sel\" href=\"$url?page=admin \" role=\"tab\">Créer un Compte</a>";
        }
        return "";
    }

    private function renderPageCrenaux($arg) {
        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getPath();
        $adminLink = $this->generateAdminLink($url);
        $urlLogout = $request->getRootUri() . "/logout";
        $qr = $this->generateCreneaux($arg['creneau']);
        return <<<END
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
      <a class="nav-link" href="$url?page= " role="tab">Page Principal</a>
      <a class="nav-link"  href="$url?page=modif" role="tab">Modifier compte</a>
      <a class="nav-link active"  href="$url?page=creneaux" role="tab">Créneaux</a>
      $adminLink
      <a class="nav-link"  href="$urlLogout" role="tab">Déconnexion</a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div>
      $qr
      </div>
    </div>
  </div>
</div>
END;
    }

    private function generateCreneaux($arg) {
        $res = "";
        foreach ($arg as $item) {
            $role = $item->role;
            $cre = $item->creneau;
            $res .= <<<END
            <div class="card">
  <h5 class="card-header">$role->label</h5>
  <div class="card-body">
    <h5 class="card-text">Vous avez participé au créneau : Semaine  $cre->semaine, cycle $cre->cycle , jour $cre->jour , heure $cre->heure</h5>
  </div>
</div>
END;
        }
        return $res;

    }

    private function renderAufMenu($arg) {
        $slim = Slim::getInstance();
        $request = $slim->request;
        $host = $request->getHost();
        $url = $request->getPath();
        $adminLink = $this->generateAdminLink($url);
        $urlLogout = $request->getRootUri() . "/logout";
        $urlcreneaux = $request->getRootUri() . "/creneau";
        return <<<END
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
      <a class="nav-link active" href="$url?page= " role="tab">Page Principal</a>
      <a class="nav-link"  href="$url?page=modif" role="tab">Modifier compte</a>
      <a class="nav-link"  href="$url?page=creneaux" role="tab">Créneaux</a>
      $adminLink
      <a class="nav-link"  href="$urlLogout" role="tab">Déconnexion</a>
    </div>
  </div>
  <div class="col-9 text-white">
    <div class="tab-content" id="v-pills-tabContent">
        Vous avez accès aux créneaux au  <a href="$urlcreneaux">lien suivant </a>
      <div>
       <div> 
       </div>
</div>
    </div>
  </div>
</div>
END;
    }

    private function renderAdmin($arg)
    {
        $urlcréaCompte = Slim::getInstance()->urlFor("newAccount");

        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getPath();
        $adminLink = $this->generateAdminLink($url, "active");
        $urlLogout = $request->getRootUri() . "/logout";
        return <<<END
<div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
      <a class="nav-link " href="$url?page= " role="tab">Page Principal</a>
      <a class="nav-link"  href="$url?page=modif" role="tab">Modifier compte</a>
      <a class="nav-link"  href="$url?page=creneaux" role="tab">Créneaux</a>
      $adminLink
      <a class="nav-link"  href="$urlLogout" role="tab">Déconnexion</a>
    </div>
  </div>
  <div class="col-9">
    <h1 class="text-dark text-black-50">Bienvenue sur la page de création d'un compte ! </h1>
    <div class="row text-white">
  <div class="col-8">
    <div class="tab-content" id="v-pills-tabContent">
      <div>
         <div class="form">
          <form class="form-check" method="post" action="$urlcréaCompte">
           <div class="form-row">
      <div class="form-group col-md-6">
      <label for="name">Nom</label>
      <input type="text" class="form-control" id="name" name="name" placeholder="Nom" required >
    </div>
    <div class="form-group col-md-6">
      <label for="prenom">Prenom</label>
      <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Prenom" required >
    </div></div>
  <div class="form-row">
      <div class="form-group col-md-6">
      <label for="login">Login</label>
      <input class="form-control disabled" disabled placeholder="login" id="login" required >
    </div>
     <div class="form-group col-md-6">
      <label for="mail">Mail</label>
      <input type="email" class="form-control" id="mail" name="mail" placeholder="Email"  " required>
    </div> </div>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="tel">Telephone</label>
      <input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" class="form-control" id="tel" name="tel" placeholder="Telephone"   onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Numéro de téléphone invalide' : '')" required >
    </div>
</div>
  <div class="form-row">
   <div class="form-group col-md-6">
      <label for="password">Mot de passe</label>
      <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" onchange="if(this.checkValidity()) form.passwordConf.pattern = this.value;">
    </div>
    <div class="form-group col-md-6">
      <label for="passwordConf">Confirmation</label>
      <input type="password" class="form-control" id="passwordConf" name="passwordConf" placeholder="Mot de passe" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Mots de passe différents' : '');">
    </div> </div>
    <button type="submit" class="btn btn-primary">Validez</button>
          </form>
        </div>
</div>
    </div>
  </div>
</div>
END;


    }
}