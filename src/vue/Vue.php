<?php


namespace charlyday\vue;


use Slim\Slim;

abstract class Vue {

    public abstract function render($sel, $arg);

    protected final function renduTitre() {
        $rootUri = Slim::getInstance()->request->getRootUri();
        return "<!DOCTYPE html><html lang='fr'>
<head>

  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
  <meta name='description' content=''>
  <meta name='author' content=''>

  <title>La grande épicerie générale Nancy</title>
    <link rel=\"stylesheet\" href='" . $rootUri . "/web/css/bootstrap.css' crossorigin=\"anonymous\">
    <script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>



  <!-- Custom fonts for this theme -->
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel=\"stylesheet\" href='" . Slim::getInstance()->urlFor("ind") . "./web/css/bootstrap.css' crossorigin=\"anonymous\">
    <script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>
    <script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>
    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>
  <!-- Theme CSS -->
  <link href='$rootUri/css/freelancer.css' rel='stylesheet'>

</head>";
    }

    protected final function renduMenu() {
        $url = $this->generateLink();
        $tr = Slim::getInstance()->request->getRootUri();
        return <<<FIN
		<nav class='navbar navbar-expand-lg bg-secondary text-uppercase ' id='mainNav'  style='background-image:url("$tr/img/generale.jpg");height:150px'>
    <div class='container'>
       <a class='navbar-brand js-scroll-trigger' href='$tr'><img src="$tr/img/logo.png" width="85px" height="85px"> </a>
      <button class='navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
        Connexion
        <i class='fas fa-bars'></i>
      </button>
      <div class='collapse navbar-collapse' id='navbarResponsive'>
        <ul class='navbar-nav ml-auto'>
          <li class='nav-item mx-0 mx-lg-1 border border-primary bg-primary rounded ' style="padding: 3px">
                $url
          </li>
        </ul>
      </div>
    </div>
  </nav>
  
FIN;

    }


    private final function generateLink() {
        $slim = Slim::getInstance();
        $request = $slim->request;
        if (!array_key_exists("id", $_SESSION)) {
            $url = $request->getRootUri() . "/connect";
            $url = "<a class='nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger'  href=\"$url\">Connexion</a>";
        } else {
            $name = $_SESSION['id']['login'];
            $t = $request->getRootUri() . '/connected';
            $url = "<a class='class=\"p-2 text-yellow\"' href='$t'>Bienvenue $name</a>";

        }
        return $url;
    }

    protected final function rendufooter() {
        return ""; //todo Make footer
    }

}