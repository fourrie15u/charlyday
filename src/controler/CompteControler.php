<?php


namespace charlyday\controler;


use charlyday\exception\AuthException;
use charlyday\model\Authentication;
use charlyday\model\FaitRole;
use charlyday\model\User;
use charlyday\vue\VueCompte;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Slim;

class CompteControler {

    public function afficheListCompte() {
        $v = new VueCompte();
        $v->render(VueCompte::MAIN_PAGE_COMPTE, ["compte" => $this->generateCompte()]);
    }


    private function generateCompte() {
        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getRootUri();
        $users = User::All();
        $res = "";
        foreach ($users as $user) {
            $res .= "<div >
        <a class='media text-muted pt-3' href='$url/connexion/$user->id'>  <img src =\"img/$user->img\" alt=\"32x32\" class=\"mr-2 rounded\" data-holder-rendered=\"true\" style=\"width: 32px; height: 32px;\">
          <p class=\"media-body pb-3 mb-0 small lh-125 border-bottom border-gray\">
            <strong class=\"d-block text-gray-dark\">@$user->nom</strong>
          </p></a>
        </div>";
        }
        return $res;
    }

    public function formConnexion($id) {
        $user = User::find($id);
        $slim = Slim::getInstance();
        $request = $slim->request;
        $url = $request->getRootUri();
        $al = $this->generateError();
        $res = <<<END
$al
<!-- Password input-->
<div class="justify-content-md-center align-self-center">
<div class="d-inline-flex col-md-4">
 <img src ="$url/img/$user->img" alt="32x32" class="mr-2 rounded" data-holder-rendered="true" style="width: 70px; height: 70px;">
 <p class="media-body pb-3 mb-0 lh-125">
 <br>
 Bienvenue $user->nom !
</p>
</div>
<form method="POST">
<div class="form-group">
  <label class="col-md-4 control-label" for="passwordName">Mot de passe</label>
  <div class="col-md-4">
    <input id="passwordID" name="password" type="password" placeholder="Mot de passe" class="form-control input-md" required="">
    <span class="help-block">Entrez votre mot de passe</span>
  </div>
</div>
<div class="form-group col-md-4">
    <input type="submit" class="btn btn-info">
</div>
</form>
</div>
</div>
END;

        $v = new VueCompte();
        $v->render(VueCompte::PAGE_AUF, $res);

    }

    private function generateError() {
        $al = "";
        if (isset($_COOKIE['Error'])) {
            $t = $_COOKIE['Error'];
            $al = "<div class=\"alert alert-danger\" role=\"alert\">$t</div>";
            setcookie('Error', "", 0);
        }
        return $al;
    }

    public function connect($id) {
        $u = User::find($id);
        $slim = Slim::getInstance();
        $pass = filter_var($_POST['password'], FILTER_SANITIZE_STRING);
        try {
            Authentication::authenticate($u->nom, $pass);
            $slim->redirectTo('connected');
        } catch (AuthException $e) {
            setcookie("Error", $e->getMessage(), time() + 10);
            $slim->redirectTo('coPass', ['id' => $id]);
        }
    }

    public function connected() {
        $slim = Slim::getInstance();
        if ($this->isConnected()) {
            $msg = "";
            if (isset($_COOKIE['Error'])) {
                $msg = $_COOKIE['Error'];
                setcookie("Error", "", 0);
            }
            $vueCompte = new VueCompte();
            $page = isset($_GET['page']) ? $_GET['page'] : "";
            $var = User::find($_SESSION['id']['uid']);
            switch ($page) {
                case "modif":
                    $vueCompte->render(VueCompte::AUFMODIFCOMPTE, ["error" => $msg, "user" => $var]);
                    break;
                case "creneaux":
                    $vueCompte->render(VueCompte::CRENAUX, ["error" => $msg, "user" => $var, "creneau" => FaitRole::where('id_user', '=', $_SESSION['id']['uid'])->distinct()->get()]);
                    break;
                case "admin":
                    if (Authentication::checkAccessRights()) {
                        $vueCompte->render(VueCompte::ADMIN, ["error" => $msg, "user" => $var]);
                        break;
                    }
                default:
                    $vueCompte->render(VueCompte::NORMAL, $msg);
                    break;
            }
        } else {
            $slim->redirect($slim->urlFor("connect"), 302);
        }
    }

    private function isConnected() {
        return isset($_SESSION['id']);
    }

    public function modifCompte() {
        $slim = Slim::getInstance();
        $url = $slim->request->getRootUri();
        if ($this->isConnected()) {
            $nom = filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS);
            $prenom = filter_var($_POST['prenom'], FILTER_SANITIZE_SPECIAL_CHARS);
            $pass = filter_var($_POST['password'], FILTER_SANITIZE_SPECIAL_CHARS);
            $passC = filter_var($_POST['passwordConf'], FILTER_SANITIZE_SPECIAL_CHARS);
            $email = $_POST['mail'];
            $tel = $_POST['tel'];
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            } else {
                setcookie("Error", "L'email n'est pas valide", time() + 10);
                $slim->redirect($url . "/connected?page=modif");
            }
            try {
                $logout = false;
                $u = User::find($_SESSION['id']['uid']);
                $u->nom = $nom;
                $u->prenom = $prenom;
                $u->mail = $email;
                $u->telephone = $tel;
                if ($pass !== "" && !password_verify($pass, $u->password)) {
                    if ($pass === $passC) {
                        $u->password = password_hash($pass, PASSWORD_DEFAULT);
                        $logout = true;
                    } else {
                        setcookie("Error", "Les mots de passe ne correspondent pas", time() + 10);
                        $slim->redirect($url . "/connected?page=modif");
                    }
                }
                $u->save();
                if ($logout) {
                    $this->deconnect();
                }
            } catch (ModelNotFoundException $e) {
                $slim->notFound();
            }

        } else {
            $slim->redirect($slim->urlFor("Error"), 301);
        }
        $slim->redirect($url . "/connected");
    }

    public function deconnect() {
        session_destroy();
        Slim::getInstance()->redirectTo("ind");
    }

    private function getCreneaux() {

    }

}