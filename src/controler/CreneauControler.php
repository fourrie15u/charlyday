<?php

namespace charlyday\controler;

use charlyday\model\Creneau;
use charlyday\vue\VueCreneau;
use Slim\Slim;

class CreneauControler
{
    public static function ajouterCreneau(int $cycle, String $semaine, int $jour, int $heure)
    {
        $c = new Creneau();
        $c->cycle = $cycle;
        $c->semaine = $semaine;
        $c->jour = $jour;
        $c->heure = $heure;
        $c->actif = true;
        $c->save();
        return Creneau::all()->last();
    }

    public static function activerCreneau(int $cycle, String $semaine, int $jour, int $heure)
    {
        $c = Creneau::where("cycle", "=", "$cycle")->where("semaine", "=", "$semaine")->where("jour", "=", "$jour")->where("heure", "=", "$heure")->first();
        $c->actif = true;
        $c->save();
    }

    public static function desactiverCreneau()
    {
        if (isset($_SESSION['id'])) {
            if (isset($_POST['des'])) {
                $id = filter_var($_POST['des'], FILTER_SANITIZE_NUMBER_INT);
                $creneau = Creneau::where('id', '=', $id)->first();
                $cycle = $creneau->cycle;
                $semaine = $creneau->semaine;
                $jour = $creneau->jour;
                $heure = $creneau->heure;
                $c = Creneau::where("cycle", "=", "$cycle")->where("semaine", "=", "$semaine")->where("jour", "=", "$jour")->where("heure", "=", "$heure")->first();
                $c->actif = false;
                $c->save();
            }
        }
    }

    public static function supprimerCreneau(int $cycle, String $semaine, int $jour, int $heure)
    {
        $c = Creneau::where("cycle", "=", "$cycle")->where("semaine", "=", "$semaine")->where("jour", "=", "$jour")->where("heure", "=", "$heure")->first();
        $c->delete();
    }

    public function renderVueCreneau(int $cycle, string $semaine) {
        if (isset($_SESSION['id'])) {
            $v = new VueCreneau();
            $v->render(VueCreneau::SEMAINE, ["creneaux" => $this->creneauxSemaine($cycle, $semaine), "cycle" => $cycle, "semaine" => $semaine]);
        } else {
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connect"));
        }
    }

    private function creneauxSemaine(int $cycle, string $semaine) {
        return Creneau::where("cycle", "=", $cycle)->where("semaine", "=", $semaine)->orderBy('heure', 'ASC')->get();
    }

    public static function nextCreneauSem(int $cycle, string $semaineActu)
    {
        if (isset($_SESSION['id'])) {
            $semaine = "A";
            switch ($semaineActu) {
                case "A" :
                    $semaine = "B";
                    break;
                case "B" :
                    $semaine = "C";
                    break;
                case "C" :
                    $semaine = "D";
                    break;
            }
            return Slim::getInstance()->urlFor("creneau", ["cycle" => $cycle, "week" => $semaine]);
        } else {
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connect"));
        }
    }

    public static function nextCreneauCycle(int $cycle, string $semaineActu)
    {
        if (isset($_SESSION['id'])) {
            return Slim::getInstance()->urlFor("creneau", ["cycle" => $cycle + 1, "week" => $semaineActu]);
        } else {
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connect"));
        }
    }

    public static function prevCreneauCycle(int $cycle, string $semaineActu)
    {
        if (isset($_SESSION['id'])) {
            if ($cycle > 1)
                return Slim::getInstance()->urlFor("creneau", ["cycle" => $cycle - 1, "week" => $semaineActu]);
            else
                return Slim::getInstance()->urlFor("creneau", ["cycle" => 1, "week" => $semaineActu]);
        } else {
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connect"));
        }
    }


    public static function prevCreneauSem(int $cycle, string $semaineActu)
    {
        if (isset($_SESSION['id'])) {
            $semaine = "D";
            switch ($semaineActu) {
                case "B" :
                    $semaine = "A";
                    break;
                case "C" :
                    $semaine = "B";
                    break;
                case "D" :
                    $semaine = "C";
                    break;
            }
            return Slim::getInstance()->urlFor("creneau", ["cycle" => $cycle, "week" => $semaine]);
        } else {
            Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connect"));
        }
    }

    public function renderNewCreneau()
    {
        $vc = new VueCreneau();
        $vc->render(VueCreneau::NEW, null);
    }
}

