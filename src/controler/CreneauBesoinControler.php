<?php


namespace charlyday\controler;


use charlyday\exception\UserAlreadyAssignedException;
use charlyday\exception\UserDontHaveRoleException;
use charlyday\model\CreneauBesoin;
use charlyday\model\User;

class CreneauBesoinControler {
    public static function ajouterCreneauBesoin(int $idCreneau, int $idRoleRequis) {
        $cb = new CreneauBesoin();

        $cb->idCreneau = $idCreneau;
        $cb->idRole = $idRoleRequis;
        $cb->save();

        return $cb;
    }

    public static function modifierBesoin(int $idCreneauBesoin, int $idNouveauRole) {
        $cb = CreneauBesoin::findById($idCreneauBesoin);

        if ($cb->idUserAssigne != null)
            throw new UserAlreadyAssignedException();

        $cb->idRole = $idNouveauRole;
        $cb->save();
    }

    public static function assignerUser(int $idCreneauBesoin, int $idUser) {
        $cb = CreneauBesoin::findById($idCreneauBesoin);

        if ($cb->idUserAssigne != null)
            throw new UserAlreadyAssignedException();

        if (!User::findById($idUser)->avoirRole($cb->idRole))
            throw new UserDontHaveRoleException();

        $cb->idUserAssigne = $idUser;
        $cb->save();
    }

    public static function retirerUser(int $idCreneauBesoin) {

        $cb = CreneauBesoin::findById($idCreneauBesoin);

        $cb->idUserAssigne = null;
        $cb->save();
    }
}