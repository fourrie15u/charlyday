<?php


namespace charlyday\model;


use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $colone, string $comparateur, $valeur)
 */
class CreneauBesoin extends Model
{
    public $timestamps = true;

    protected $table = 'creneauBesoin';
    protected $primaryKey = 'id';

    const CREATED_AT = 'dateCreation';
    const UPDATED_AT = 'dateModification';

    public static function findById(int $id)
    {
        return CreneauBesoin::where("id", "=", $id)->first();
    }

    public function role(){
        return $this->belongsTo('\charlyday\model\role','idRole');
    }

}