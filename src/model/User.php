<?php


namespace charlyday\model;


use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $colone, string $comparateur, $value)
 */
class User extends Model {
    const CREATED_AT = 'dateCreation';
    const UPDATED_AT = 'dateModification';
    public $timestamps = true;
    protected $table = 'user';
    protected $primaryKey = 'id';

    public static function findById(int $id) {
        return User::where("id", "=", $id)->first();
    }

    public function roles() {
        return $this->hasMany('\charlyday\model\FaitRole', 'id_role');
    }
}