<?php

namespace charlyday\model;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, string $string1, string $string2)
 */
class Creneau extends Model {
    const CREATED_AT = 'dateCreation';
    const UPDATED_AT = 'dateModification';
    public $timestamps = true;
    protected $table = 'creneau';
    protected $primaryKey = 'id';




    public function creneaux(){
        return $this->hasMany('\charlyday\model\creneauBesoin', 'idCreneau');
    }
}
