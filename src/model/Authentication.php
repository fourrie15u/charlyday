<?php

namespace charlyday\model;

use charlyday\exception\AuthException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Authentication {

    /**
     * @throws AuthException l'erreur lors de la connexion
     */
    public static function createUser($password, $nom, $prenom, $mail, $tel)
    {
        if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
            $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
        } else {
            throw new AuthException("Email invalide");
        }

        $user = new User();
        //$user->login = $userName;
        $user->mail = $mail;
        $user->nom = $nom;
        $user->prenom = $prenom;
        $user->password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
        $user->telephone = $tel;
        $user->save();
        self::loadProfile($user->id);
    }

    private static function generateUID() {
        do {
            $uid = bin2hex(openssl_random_pseudo_bytes(32));
        } while (User::where('uid', "=", $uid)->first() !== null);
        return $uid;
    }

    /**
     * @param $uid String l'uid
     * @throws AuthException si il y a une erreur lors de la charge du profil
     */
    private static function loadProfile($uid) {
        $u = User::find($uid);
        if ($u === null) {
            throw new AuthException('Erreur lors de la connexion');
        }
        unset($_SESSION['id']);
        $_SESSION['id'] = ["login" => $u->nom, "uid" => $uid];
    }

    public static function authenticate($username, $password) {
        try {
            $u = User::where("nom", "=", $username)->firstOrFail();
            if (password_verify($password, $u->password)) {
                self::loadProfile($u->id);
            } else {
                throw new AuthException("Erreur lors de l'authentification");
            }
        } catch (ModelNotFoundException $ignored) {
            throw new AuthException("Erreur lors de l'authentification");
        }
    }

    public static function checkAccessRights($u = null) {
        if ($u === null) {
            $u = User::find($_SESSION['id']['uid']);
            if ($u === null)
                return false;
        }
        return $u->admin == 1;

    }

    public static function aLerole($role){
        $frs=FaitRole::where('id_user','=',$_SESSION['id']['uid'])->distinct()->get();
        $bool=false;
        $res="";
        $i=0;
        while(!$bool&&$i<$frs->count()){
            if($frs[$i]->id_role==$role->besoinForm){
                $bool=true;
                $res=$role->label;

            }
            else $i++;
        }
        return $res;
    }
}