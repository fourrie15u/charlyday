<?php

namespace charlyday\model;

use Illuminate\Database\Eloquent\Model;
class Role extends Model
{
    public $timestamps = true;
    protected $table = 'role';
    protected $primaryKey = 'id';

    const CREATED_AT = 'dateCreation';
    const UPDATED_AT = 'dateModification';



}