<?php

namespace charlyday\model;

use Illuminate\Database\Eloquent\Model;

class FaitRole extends Model {
    const CREATED_AT = 'dateCreation';
    const UPDATED_AT = 'dateModification';
    public $timestamps = true;
    protected $table = 'faitRole';
    protected $primaryKey = 'id';

    public function user() {
        return $this->belongsTo('\charlyday\model\faitRole', 'id_user');
    }

    public function role() {
        return $this->belongsTo('\charlyday\model\Role', 'id_role');
    }

    public function creneau() {
        return $this->belongsTo('\charlyday\model\Creneau', 'id');
    }
}
