<?php


use charlyday\controler\CompteControler;
use charlyday\controler\CreneauBesoinControler;
use charlyday\controler\CreneauControler;
use charlyday\controler\RoleControler;
use charlyday\model\Authentication;
use charlyday\vue\VueIndex;
use Illuminate\Database\Capsule\Manager as DB;
use Slim\Slim;


require_once __DIR__ . '/vendor/autoload.php';
session_start();
$app = new Slim;

$app->get('/', function () {
    $vueIndex = new VueIndex();
    $vueIndex->render(1, null);
})->setName('ind');

$app->get('/connect', function () {
    $cCont = new CompteControler();
    $cCont->afficheListCompte();
})->setName("connect");

$app->get('/connexion/:id', function ($id) {
    $cCont = new CompteControler();
    $cCont->formConnexion($id);
})->setName('coPass');

$app->post('/connexion/:id', function ($id) {
    $cCont = new CompteControler();
    $cCont->connect($id);
});
$app->get('/connected', function () {
    $cCont = new CompteControler();
    $cCont->connected();
})->setName("connected");

$app->get('/logout', function () {
    $c = new CompteControler();
    $c->deconnect();
});
$app->get('/creneau(/:cycle(/:week))', function (int $cycle = 1, string $week = 'A') {
    $crenCont = new CreneauControler();
    $crenCont->renderVueCreneau($cycle, $week);
})->name("creneau");

$app->get('/newCreneau', function () {
    $crenCont = new CreneauControler();
    $crenCont->renderNewCreneau();
})->name("newCreneau");

$app->post('/inscription', function () {
    $crenRoleCon=new RoleControler();
    $crenRoleCon->inscrire();
})->name("/inscription");;

$app->post('/modifcompte', function () {
    $cCont = new CompteControler();
    $cCont->modifCompte();
});

$app->post('/desactive', function () {
    $cCont = new CreneauControler();
    $cCont->desactiverCreneau();
});

$app->post('/newCreneau/process', function () {
    CreneauControler::ajouterCreneau(1, $_POST["semaine"], $_POST["jour"], $_POST["heure"]);
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("creneau"));
})->setName("newCreneauProcess");

$app->post('/newAccount', function () {
    Authentication::createUser($_POST["password"], $_POST["name"], $_POST["prenom"], $_POST["mail"], $_POST["tel"]);
    Slim::getInstance()->redirect(Slim::getInstance()->urlFor("connected"));
})->setName("newAccount");

$db = new DB();
$db->addConnection(parse_ini_file("src/conf/conf.ini"));
$db->setAsGlobal();
$db->bootEloquent();

$app->run();

